define(['exports', 'module', 'knockout', 'text!./home.html'], function (exports, module, _knockout, _textHomeHtml) {
    'use strict';

    var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

    var _ko = _interopRequireDefault(_knockout);

    var _homeTemplate = _interopRequireDefault(_textHomeHtml);

    var HomeViewModel = (function () {
        function HomeViewModel(route) {
            _classCallCheck(this, HomeViewModel);

            this.message = _ko['default'].observable('Welcome to Lobby!');
        }

        _createClass(HomeViewModel, [{
            key: 'doSomething',
            value: function doSomething() {
                this.message('You invoked doSomething() on the viewmodel.');
            }
        }]);

        return HomeViewModel;
    })();

    module.exports = { viewModel: HomeViewModel, template: _homeTemplate['default'] };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9zb3VyY2UvY29tcG9uZW50c1xcaG9tZS1wYWdlXFxob21lLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7UUFHTSxhQUFhO0FBQ0osaUJBRFQsYUFBYSxDQUNILEtBQUssRUFBRTtrQ0FEakIsYUFBYTs7QUFFWCxnQkFBSSxDQUFDLE9BQU8sR0FBRyxlQUFHLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQ3JEOztxQkFIQyxhQUFhOzttQkFLSix1QkFBRztBQUNWLG9CQUFJLENBQUMsT0FBTyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7YUFDL0Q7OztlQVBDLGFBQWE7OztxQkFVSixFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsUUFBUSwwQkFBYyxFQUFFIiwiZmlsZSI6InNyYy9jb21wb25lbnRzL2hvbWUtcGFnZS9ob21lLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGtvIGZyb20gJ2tub2Nrb3V0JztcbmltcG9ydCBob21lVGVtcGxhdGUgZnJvbSAndGV4dCEuL2hvbWUuaHRtbCc7XG5cbmNsYXNzIEhvbWVWaWV3TW9kZWwge1xuICAgIGNvbnN0cnVjdG9yKHJvdXRlKSB7XG4gICAgICAgIHRoaXMubWVzc2FnZSA9IGtvLm9ic2VydmFibGUoJ1dlbGNvbWUgdG8gTG9iYnkhJyk7XG4gICAgfVxuICAgIFxuICAgIGRvU29tZXRoaW5nKCkge1xuICAgICAgICB0aGlzLm1lc3NhZ2UoJ1lvdSBpbnZva2VkIGRvU29tZXRoaW5nKCkgb24gdGhlIHZpZXdtb2RlbC4nKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHsgdmlld01vZGVsOiBIb21lVmlld01vZGVsLCB0ZW1wbGF0ZTogaG9tZVRlbXBsYXRlIH07Il19