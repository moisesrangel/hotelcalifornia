define(['exports', 'module', 'knockout', 'crossroads', 'hasher'], function (exports, module, _knockout, _crossroads, _hasher) {
    'use strict';

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

    var _ko = _interopRequireDefault(_knockout);

    var _crossroads2 = _interopRequireDefault(_crossroads);

    var _hasher2 = _interopRequireDefault(_hasher);

    // This module configures crossroads.js, a routing library. If you prefer, you
    // can use any other routing library (or none at all) as Knockout is designed to
    // compose cleanly with external libraries.
    //
    // You *don't* have to follow the pattern established here (each route entry
    // specifies a 'page', which is a Knockout component) - there's nothing built into
    // Knockout that requires or even knows about this technique. It's just one of
    // many possible ways of setting up client-side routes.

    var Router = function Router(config) {
        var _this = this;

        _classCallCheck(this, Router);

        this.currentRoute = _ko['default'].observable({});

        // Configure Crossroads route handlers
        _ko['default'].utils.arrayForEach(config.routes, function (route) {
            _crossroads2['default'].addRoute(route.url, function (requestParams) {
                _this.currentRoute(_ko['default'].utils.extend(requestParams, route.params));
            });
        });

        // Activate Crossroads
        _crossroads2['default'].normalizeFn = _crossroads2['default'].NORM_AS_OBJECT;
        _hasher2['default'].initialized.add(function (hash) {
            return _crossroads2['default'].parse(hash);
        });
        _hasher2['default'].changed.add(function (hash) {
            return _crossroads2['default'].parse(hash);
        });
        _hasher2['default'].init();
    }

    // Create and export router instance
    ;

    var routerInstance = new Router({
        routes: [{ url: '', params: { page: 'home-page' } }, { url: 'about', params: { page: 'about-page' } }]
    });

    module.exports = routerInstance;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9zb3VyY2UvYXBwXFxyb3V0ZXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQWFNLE1BQU0sR0FDRyxTQURULE1BQU0sQ0FDSSxNQUFNLEVBQUU7Ozs4QkFEbEIsTUFBTTs7QUFFSixZQUFJLENBQUMsWUFBWSxHQUFHLGVBQUcsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7QUFHdEMsdUJBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFVBQUMsS0FBSyxFQUFLO0FBQzVDLG9DQUFXLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLFVBQUMsYUFBYSxFQUFLO0FBQzlDLHNCQUFLLFlBQVksQ0FBQyxlQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2FBQ25FLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQzs7O0FBR0gsZ0NBQVcsV0FBVyxHQUFHLHdCQUFXLGNBQWMsQ0FBQztBQUNuRCw0QkFBTyxXQUFXLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTttQkFBSSx3QkFBVyxLQUFLLENBQUMsSUFBSSxDQUFDO1NBQUEsQ0FBQyxDQUFDO0FBQ3ZELDRCQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO21CQUFJLHdCQUFXLEtBQUssQ0FBQyxJQUFJLENBQUM7U0FBQSxDQUFDLENBQUM7QUFDbkQsNEJBQU8sSUFBSSxFQUFFLENBQUM7S0FDakI7Ozs7O0FBSUwsUUFBSSxjQUFjLEdBQUcsSUFBSSxNQUFNLENBQUM7QUFDNUIsY0FBTSxFQUFFLENBQ0osRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFXLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsRUFBRSxFQUNuRCxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQU0sTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQ3ZEO0tBQ0osQ0FBQyxDQUFDOztxQkFFWSxjQUFjIiwiZmlsZSI6InNyYy9hcHAvcm91dGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGtvIGZyb20gJ2tub2Nrb3V0JztcbmltcG9ydCBjcm9zc3JvYWRzIGZyb20gJ2Nyb3Nzcm9hZHMnO1xuaW1wb3J0IGhhc2hlciBmcm9tICdoYXNoZXInO1xuXG4vLyBUaGlzIG1vZHVsZSBjb25maWd1cmVzIGNyb3Nzcm9hZHMuanMsIGEgcm91dGluZyBsaWJyYXJ5LiBJZiB5b3UgcHJlZmVyLCB5b3Vcbi8vIGNhbiB1c2UgYW55IG90aGVyIHJvdXRpbmcgbGlicmFyeSAob3Igbm9uZSBhdCBhbGwpIGFzIEtub2Nrb3V0IGlzIGRlc2lnbmVkIHRvXG4vLyBjb21wb3NlIGNsZWFubHkgd2l0aCBleHRlcm5hbCBsaWJyYXJpZXMuXG4vL1xuLy8gWW91ICpkb24ndCogaGF2ZSB0byBmb2xsb3cgdGhlIHBhdHRlcm4gZXN0YWJsaXNoZWQgaGVyZSAoZWFjaCByb3V0ZSBlbnRyeVxuLy8gc3BlY2lmaWVzIGEgJ3BhZ2UnLCB3aGljaCBpcyBhIEtub2Nrb3V0IGNvbXBvbmVudCkgLSB0aGVyZSdzIG5vdGhpbmcgYnVpbHQgaW50b1xuLy8gS25vY2tvdXQgdGhhdCByZXF1aXJlcyBvciBldmVuIGtub3dzIGFib3V0IHRoaXMgdGVjaG5pcXVlLiBJdCdzIGp1c3Qgb25lIG9mXG4vLyBtYW55IHBvc3NpYmxlIHdheXMgb2Ygc2V0dGluZyB1cCBjbGllbnQtc2lkZSByb3V0ZXMuXG5cbmNsYXNzIFJvdXRlciB7XG4gICAgY29uc3RydWN0b3IoY29uZmlnKSB7XG4gICAgICAgIHRoaXMuY3VycmVudFJvdXRlID0ga28ub2JzZXJ2YWJsZSh7fSk7XG4gICAgXG4gICAgICAgIC8vIENvbmZpZ3VyZSBDcm9zc3JvYWRzIHJvdXRlIGhhbmRsZXJzXG4gICAgICAgIGtvLnV0aWxzLmFycmF5Rm9yRWFjaChjb25maWcucm91dGVzLCAocm91dGUpID0+IHtcbiAgICAgICAgICAgIGNyb3Nzcm9hZHMuYWRkUm91dGUocm91dGUudXJsLCAocmVxdWVzdFBhcmFtcykgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFJvdXRlKGtvLnV0aWxzLmV4dGVuZChyZXF1ZXN0UGFyYW1zLCByb3V0ZS5wYXJhbXMpKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICBcbiAgICAgICAgLy8gQWN0aXZhdGUgQ3Jvc3Nyb2Fkc1xuICAgICAgICBjcm9zc3JvYWRzLm5vcm1hbGl6ZUZuID0gY3Jvc3Nyb2Fkcy5OT1JNX0FTX09CSkVDVDtcbiAgICAgICAgaGFzaGVyLmluaXRpYWxpemVkLmFkZChoYXNoID0+IGNyb3Nzcm9hZHMucGFyc2UoaGFzaCkpO1xuICAgICAgICBoYXNoZXIuY2hhbmdlZC5hZGQoaGFzaCA9PiBjcm9zc3JvYWRzLnBhcnNlKGhhc2gpKTtcbiAgICAgICAgaGFzaGVyLmluaXQoKTtcbiAgICB9XG59XG5cbi8vIENyZWF0ZSBhbmQgZXhwb3J0IHJvdXRlciBpbnN0YW5jZVxudmFyIHJvdXRlckluc3RhbmNlID0gbmV3IFJvdXRlcih7XG4gICAgcm91dGVzOiBbXG4gICAgICAgIHsgdXJsOiAnJywgICAgICAgICAgcGFyYW1zOiB7IHBhZ2U6ICdob21lLXBhZ2UnIH0gfSxcbiAgICAgICAgeyB1cmw6ICdhYm91dCcsICAgICBwYXJhbXM6IHsgcGFnZTogJ2Fib3V0LXBhZ2UnIH0gfVxuICAgIF1cbn0pO1xuXG5leHBvcnQgZGVmYXVsdCByb3V0ZXJJbnN0YW5jZTtcbiJdfQ==