define(['exports', 'jquery', 'bootstrap', 'knockout', 'knockout-projections', './router'], function (exports, _jquery, _bootstrap, _knockout, _knockoutProjections, _router) {
    'use strict';

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

    var _ko = _interopRequireDefault(_knockout);

    // Components can be packaged as AMD modules, such as the following:
    _ko['default'].components.register('nav-bar', { require: 'components/nav-bar/nav-bar' });
    _ko['default'].components.register('home-page', { require: 'components/home-page/home' });

    // ... or for template-only components, you can just point to a .html file directly:
    _ko['default'].components.register('about-page', {
        template: { require: 'text!components/about-page/about.html' }
    });

    // [Scaffolded component registrations will be inserted here. To retain this feature, don't remove this comment.]

    // Start the application
    _ko['default'].applyBindings({ route: _router.currentRoute });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9zb3VyY2UvYXBwXFxzdGFydHVwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBT0EsbUJBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsQ0FBQyxDQUFDO0FBQzdFLG1CQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLENBQUMsQ0FBQzs7O0FBRzlFLG1CQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFO0FBQ2pDLGdCQUFRLEVBQUUsRUFBRSxPQUFPLEVBQUUsdUNBQXVDLEVBQUU7S0FDakUsQ0FBQyxDQUFDOzs7OztBQUtILG1CQUFHLGFBQWEsQ0FBQyxFQUFFLEtBQUssRUFBRSxRQUFPLFlBQVksRUFBRSxDQUFDLENBQUMiLCJmaWxlIjoic3JjL2FwcC9zdGFydHVwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICdqcXVlcnknO1xuaW1wb3J0ICdib290c3RyYXAnO1xuaW1wb3J0IGtvIGZyb20gJ2tub2Nrb3V0JztcbmltcG9ydCAna25vY2tvdXQtcHJvamVjdGlvbnMnXG5pbXBvcnQgKiBhcyByb3V0ZXIgZnJvbSAnLi9yb3V0ZXInO1xuXG4vLyBDb21wb25lbnRzIGNhbiBiZSBwYWNrYWdlZCBhcyBBTUQgbW9kdWxlcywgc3VjaCBhcyB0aGUgZm9sbG93aW5nOlxua28uY29tcG9uZW50cy5yZWdpc3RlcignbmF2LWJhcicsIHsgcmVxdWlyZTogJ2NvbXBvbmVudHMvbmF2LWJhci9uYXYtYmFyJyB9KTtcbmtvLmNvbXBvbmVudHMucmVnaXN0ZXIoJ2hvbWUtcGFnZScsIHsgcmVxdWlyZTogJ2NvbXBvbmVudHMvaG9tZS1wYWdlL2hvbWUnIH0pO1xuXG4vLyAuLi4gb3IgZm9yIHRlbXBsYXRlLW9ubHkgY29tcG9uZW50cywgeW91IGNhbiBqdXN0IHBvaW50IHRvIGEgLmh0bWwgZmlsZSBkaXJlY3RseTpcbmtvLmNvbXBvbmVudHMucmVnaXN0ZXIoJ2Fib3V0LXBhZ2UnLCB7XG4gICAgdGVtcGxhdGU6IHsgcmVxdWlyZTogJ3RleHQhY29tcG9uZW50cy9hYm91dC1wYWdlL2Fib3V0Lmh0bWwnIH1cbn0pO1xuXG4vLyBbU2NhZmZvbGRlZCBjb21wb25lbnQgcmVnaXN0cmF0aW9ucyB3aWxsIGJlIGluc2VydGVkIGhlcmUuIFRvIHJldGFpbiB0aGlzIGZlYXR1cmUsIGRvbid0IHJlbW92ZSB0aGlzIGNvbW1lbnQuXVxuXG4vLyBTdGFydCB0aGUgYXBwbGljYXRpb25cbmtvLmFwcGx5QmluZGluZ3MoeyByb3V0ZTogcm91dGVyLmN1cnJlbnRSb3V0ZSB9KTtcbiJdfQ==