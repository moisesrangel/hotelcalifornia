define(["exports"], function (exports) {
    // require.js looks for the following global when initializing
    "use strict";

    var require = {
        baseUrl: ".",
        paths: {
            "bootstrap": "bower_modules/components-bootstrap/js/bootstrap.min",
            "crossroads": "bower_modules/crossroads/dist/crossroads.min",
            "hasher": "bower_modules/hasher/dist/js/hasher.min",
            "jquery": "bower_modules/jquery/dist/jquery",
            "knockout": "bower_modules/knockout/dist/knockout",
            "knockout-projections": "bower_modules/knockout-projections/dist/knockout-projections",
            "signals": "bower_modules/js-signals/dist/signals.min",
            "text": "bower_modules/requirejs-text/text"
        },
        shim: {
            "bootstrap": { deps: ["jquery"] }
        }
    };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9zb3VyY2UvYXBwXFxyZXF1aXJlLmNvbmZpZy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsUUFBSSxPQUFPLEdBQUc7QUFDVixlQUFPLEVBQUUsR0FBRztBQUNaLGFBQUssRUFBRTtBQUNILHVCQUFXLEVBQWEscURBQXFEO0FBQzdFLHdCQUFZLEVBQVksOENBQThDO0FBQ3RFLG9CQUFRLEVBQWdCLHlDQUF5QztBQUNqRSxvQkFBUSxFQUFnQixrQ0FBa0M7QUFDMUQsc0JBQVUsRUFBYyxzQ0FBc0M7QUFDOUQsa0NBQXNCLEVBQUUsOERBQThEO0FBQ3RGLHFCQUFTLEVBQWUsMkNBQTJDO0FBQ25FLGtCQUFNLEVBQWtCLG1DQUFtQztTQUM5RDtBQUNELFlBQUksRUFBRTtBQUNGLHVCQUFXLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRTtTQUNwQztLQUNKLENBQUMiLCJmaWxlIjoic3JjL2FwcC9yZXF1aXJlLmNvbmZpZy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIHJlcXVpcmUuanMgbG9va3MgZm9yIHRoZSBmb2xsb3dpbmcgZ2xvYmFsIHdoZW4gaW5pdGlhbGl6aW5nXG52YXIgcmVxdWlyZSA9IHtcbiAgICBiYXNlVXJsOiBcIi5cIixcbiAgICBwYXRoczoge1xuICAgICAgICBcImJvb3RzdHJhcFwiOiAgICAgICAgICAgIFwiYm93ZXJfbW9kdWxlcy9jb21wb25lbnRzLWJvb3RzdHJhcC9qcy9ib290c3RyYXAubWluXCIsXG4gICAgICAgIFwiY3Jvc3Nyb2Fkc1wiOiAgICAgICAgICAgXCJib3dlcl9tb2R1bGVzL2Nyb3Nzcm9hZHMvZGlzdC9jcm9zc3JvYWRzLm1pblwiLFxuICAgICAgICBcImhhc2hlclwiOiAgICAgICAgICAgICAgIFwiYm93ZXJfbW9kdWxlcy9oYXNoZXIvZGlzdC9qcy9oYXNoZXIubWluXCIsXG4gICAgICAgIFwianF1ZXJ5XCI6ICAgICAgICAgICAgICAgXCJib3dlcl9tb2R1bGVzL2pxdWVyeS9kaXN0L2pxdWVyeVwiLFxuICAgICAgICBcImtub2Nrb3V0XCI6ICAgICAgICAgICAgIFwiYm93ZXJfbW9kdWxlcy9rbm9ja291dC9kaXN0L2tub2Nrb3V0XCIsXG4gICAgICAgIFwia25vY2tvdXQtcHJvamVjdGlvbnNcIjogXCJib3dlcl9tb2R1bGVzL2tub2Nrb3V0LXByb2plY3Rpb25zL2Rpc3Qva25vY2tvdXQtcHJvamVjdGlvbnNcIixcbiAgICAgICAgXCJzaWduYWxzXCI6ICAgICAgICAgICAgICBcImJvd2VyX21vZHVsZXMvanMtc2lnbmFscy9kaXN0L3NpZ25hbHMubWluXCIsXG4gICAgICAgIFwidGV4dFwiOiAgICAgICAgICAgICAgICAgXCJib3dlcl9tb2R1bGVzL3JlcXVpcmVqcy10ZXh0L3RleHRcIlxuICAgIH0sXG4gICAgc2hpbToge1xuICAgICAgICBcImJvb3RzdHJhcFwiOiB7IGRlcHM6IFtcImpxdWVyeVwiXSB9XG4gICAgfVxufTtcbiJdfQ==