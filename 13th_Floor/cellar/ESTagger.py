#http://www.clips.ua.ac.be/pages/using-wikicorpus-nltk-to-build-a-spanish-part-of-speech-tagger

from glob import glob
from codecs import open, BOM_UTF8
from collections import defaultdict

def wikicorpus(words=1000000, start=0):
    s = [[]]
    i = 0
    for f in glob("corpora/tagged.es/*")[start:]:
        for line in open(f, encoding="latin-1"):
            if line == "\n" or line.startswith((
              "<doc", "</doc>", "ENDOFARTICLE", "REDIRECT",
              "Acontecimientos",
              "Fallecimientos",
              "Nacimientos")):
                continue
            w, lemma, tag, x = line.split(" ")
            if tag.startswith("Fp"):
                tag = tag[:3]
            elif tag.startswith("V"):  # VMIP3P0 => VMI
                tag = tag[:3]
            elif tag.startswith("NC"): # NCMS000 => NCS
                tag = tag[:2] + tag[3]
            else:
                tag = tag[:2]
            for w in w.split("_"): # Puerto_Rico
                s[-1].append((w, tag)); i+=1
            if tag == "Fp" and w == ".":
                s.append([])
            if i >= words:
                return s[:-1]


lexicon = defaultdict(lambda: defaultdict(int))
for sentence in wikicorpus(1000000):
    for w, tag in sentence:
        lexicon[w][tag] += 1

top = []
for w, tags in lexicon.items():
    freq = sum(tags.values())      # 3741 + 243 + ...
    tag  = max(tags, key=tags.get) # DA
    top.append((freq, w, tag))

top = sorted(top, reverse=True)[:100000] # top 100,000
top = ["%s %s" % (w, tag) for freq, w, tag in top if w]

open("es-lexicon.txt", "w").write(BOM_UTF8 + "\n".join(top).encode("utf-8"))


