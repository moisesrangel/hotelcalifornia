from pattern.web     import URL, extension, download, find_urls, plaintext, Twitter
from pattern.es      import parse, pprint
from nltk.tokenize   import sent_tokenize

class Reader(object):

    text     =  ""
    deep     = 0
    urls     = []
    keywords = []

    def __init__(self, link, deep = 0):
        self.deep = deep
        self.ReadLink(link)

    def CleanEmptyLines(self, line):
        out = ""
        for l in line.split('\n'):
            if l.strip():
                out +=  l + '\n'
        return out

    def ToUTF8(self, text):
        return u''.join(text).encode('utf-8').strip()

    def FilterTokens(self, tokens):
        """
          Tags permitidos

          JJ: adjective or numeral, ordinal
          NNS: noun, common, plural
          VBP: verb, present tense, not 3rd person singular

          FW: Foreign Word // regularmente palabras localizadas

        """
        valid_tokens = ["VB", "CD", "JJ", "FW"]

        term = ""

        for token in tokens:
            for word in token:
                for tag in valid_tokens:
                    if(word[1].startswith(tag)):
                        term = term + word[0] + " "

        if term not in self.keywords:
            self.keywords.append(term)


    def Tokenizr(self, sentence):
        self.FilterTokens(parse(sentence).split())

    def GetKeyWords(self, sentences):
        for sen in sentences:
            self.Tokenizr(sen.strip())

        #print self.keywords

    def GetTweets(self):
        t = Twitter()
        for term in self.keywords:
            for tweet in t.search(term):
                print  self.ToUTF8(tweet.text)

    def ReadLink(self, link):
        html      = download(link, unicode=True)
        plain     = plaintext(html)
        plain     = self.CleanEmptyLines(plain)
        self.urls = find_urls(html, unique=True)
        sentences = sent_tokenize(plain)

        self.GetKeyWords(sentences)


if __name__ == '__main__':
    reader = Reader("http://www.vanguardia.com.mx/articulo/para-que-servira-el-911")
    reader.GetTweets()




