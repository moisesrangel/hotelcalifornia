import sys
import settings
import tim
import nltk
import requests
import os
import bleach

from slugify import slugify

from bs4 import BeautifulSoup
from models import Content, Keywords

from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
from nltk.tokenize import word_tokenize

class Jim(object):

    links         = []
    levels        = 5
    current_level = 0

    """docstring for Jim"""
    def __init__(self):
        super(Jim, self).__init__()
        self.terms()

    def terms(self):
         self.keywords = [row.keyword for row in Keywords.select()]

    def filterhref(self, link):
        href     = link.get('href')
        filters  = ['jpg', 'jpeg', 'gif', 'png', 'pdf', 'xml', 'bmp']
        status   = True
        for filtero in filters:
            print filtero
            if href.endswith(filtero):
                status = False
                break

        if status == True:
            self.links.append(link)

    def getLinks(self, soup):
        for link in soup.find_all('a'):
            self.filterhref(link)

    def soupify(self, link):
        try:
            request = requests.get(link)
            html    = request.text
            return BeautifulSoup(html)
        except Exception, e:
            print e
            pass

    def saveword(self, word):
        word = slugify(word)
        if not word:
            return

        key = Keywords()
        key.keyword = word
        key.save()
        print "In: " + word
        #print "I know: " + word

    def is_valid_url(self, url):
        import re
        regex = re.compile(
            r'^https?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        return url is not None and regex.search(url)

    def validateWord(self, word):

        status = True
        if word.isdigit():
            status = False

        if word[0].isdigit():
            status = False

        if len(word) < 4:
            status = False

        if self.is_valid_url(word):
            status = False

        if status:
            keyw = Keywords.findone(keyword = word)
            if keyw:
                status = False

        return status


    def processsentence(self, sentence):
        try:
            words = word_tokenize(sentence)
            for word in words:
                if(self.validateWord(word)):
                    self.saveword(word)
                else:
                    print "Out: " + word
        except Exception, e:
            print e
            pass


    #http://stackoverflow.com/questions/14095971/how-to-tweak-the-nltk-sentence-tokenizer
    def remember(self, text):
        book =  u''.join(text).encode('utf-8').strip()
        punkt_param = PunktParameters()
        punkt_param.abbrev_types = set(['dr', 'vs', 'mr', 'mrs', 'prof', 'inc', 'sr', 'sra', 'miss'])
        sentence_splitter = PunktSentenceTokenizer(punkt_param)
        sentences = sentence_splitter.tokenize(text)
        for sentence in sentences:
            line = u''.join(sentence).encode('utf-8').strip(' \t\n\r')
            line = os.linesep.join([s for s in line.splitlines() if s])
            self.processsentence(line)

    def readabook(self, link_book):

        print "reading " + link_book

        self.readbook = self.soupify(link_book)
        self.getLinks(self.readbook)
        self.remember(self.readbook.text)
        self.current_level  = self.current_level + 1

        if(self.levels > self.current_level):
           for link in self.links:
            try:
               self.readabook(link.get('href'))
            except Exception, e:
                print e
                pass


    def gostoen(self):
        stoen = tim.Tim()
        for term in self.keywords:
            stoen.search(term)

if __name__ == '__main__':
    print "Welcome to Jonstown, Im Jim Jones :)"
    jim = Jim()
    #jim.readabook("http://www.jornada.unam.mx/")
    jim.gostoen()



