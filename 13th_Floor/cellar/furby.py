import settings
import tweepy

from skylark import fn
from datetime import datetime

import aiml

from models import Bots,ReplyLog,Content

class Furby(object):
	def __init__(self):
		super(Furby , self).__init__()

	def talk(self, access_token, access_token_secret,last_reply,bot_id,k):
		auth 		= tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)
		auth.secure = True
		auth.set_access_token(access_token, access_token_secret)
		api 		= tweepy.API(auth)


		bot = Bots.where(Bots.id != bot_id).orderby(fn.rand(), desc=True).getone()
		tweet = Content.orderby(fn.rand(), desc=True).getone()

		try:
			api.update_status(status=tweet.text,place_id=116545)
			print bot_id
		except Exception, e:
			print e
			pass

		try:
			self.mention(api, k.respond(str(unicode(bot.name).encode("utf-8"))), bot.username)
		except Exception, e:
			pass

		try:
			for mention in api.mentions_timeline(since_id=last_reply,count=1):
				if mention.in_reply_to_status_id is None:
					self.reply(api, mention, k.respond(str(unicode(mention.text).encode("utf-8"))),bot_id)
				else:
					print 'Nones para los preguntones'
					query = Bots.at(bot_id).update(last_reply=mention.id)
					query.execute()
		except Exception, e:
			print e
			pass

	def reply(self,api,m , text, bot_id):
		text = '@' + str(unicode(m.author.screen_name).encode("utf-8")) + ' ' + text
		try:
			r = api.update_status(status=text,in_reply_to_status_id=m.id)
			query = Bots.at(bot_id).update(last_reply=m.id)
			query.execute()

			log = ReplyLog(bot_id=r.author.id, in_reply_to_status_id=m.id,in_reply_to_user_id=m.author.id,in_reply_to_screen_name=m.author.screen_name,text=m.text,reply_text=r.text,reply_date=datetime.now())

			log.save()
		except Exception, e:
			print e
			pass


	def mention(self,api, text, screen_name):
		text = '@' + str(unicode(screen_name).encode("utf-8")) + ' ' + text
		try:
			api.update_status(status=text)
		except Exception, e:
			print e
			pass