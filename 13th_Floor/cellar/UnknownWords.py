from nltk.tag import UnigramTagger
from nltk.tag import FastBrillTaggerTrainer

from nltk.tag.brill import SymmetricProximateTokensTemplate
from nltk.tag.brill import ProximateTokensTemplate
from nltk.tag.brill import ProximateTagsRule
from nltk.tag.brill import ProximateWordsRule

from glob import glob
from codecs import open, BOM_UTF8
from collections import defaultdict

def wikicorpus(words=1000000, start=0):
    s = [[]]
    i = 0
    for f in glob("corpora/tagged.es/*")[start:]:
        for line in open(f, encoding="latin-1"):
            if line == "\n" or line.startswith((
              "<doc", "</doc>", "ENDOFARTICLE", "REDIRECT",
              "Acontecimientos",
              "Fallecimientos",
              "Nacimientos")):
                continue
            w, lemma, tag, x = line.split(" ")
            if tag.startswith("Fp"):
                tag = tag[:3]
            elif tag.startswith("V"):  # VMIP3P0 => VMI
                tag = tag[:3]
            elif tag.startswith("NC"): # NCMS000 => NCS
                tag = tag[:2] + tag[3]
            else:
                tag = tag[:2]
            for w in w.split("_"): # Puerto_Rico
                s[-1].append((w, tag)); i+=1
            if tag == "Fp" and w == ".":
                s.append([])
            if i >= words:
                return s[:-1]


# {"mente": {"RG": 4860, "SP": 8, "VMS": 7}}
suffix = defaultdict(lambda: defaultdict(int))

for sentence in wikicorpus(1000000):
    for w, tag in sentence:
        x = w[-5:] # Last 5 characters.
        if len(x) < len(w) and tag != "NP":
            suffix[x][tag] += 1

top = []
for x, tags in suffix.items():
    tag = max(tags, key=tags.get) # RG
    f1  = sum(tags.values())      # 4860 + 8 + 7
    f2  = tags[tag] / float(f1)   # 4860 / 4875
    top.append((f1, f2, x, tag))

top = sorted(top, reverse=True)
top = filter(lambda (f1, f2, x, tag): f1 >= 10 and f2 > 0.8, top)
top = filter(lambda (f1, f2, x, tag): tag != "NCS", top)
top = top[:100]
top = ["%s %s fhassuf %s %s" % ("NCS", x, len(x), tag) for f1, f2, x, tag in top]

open("es-morphology.txt", "w").write(BOM_UTF8 + "\n".join(top).encode("utf-8"))