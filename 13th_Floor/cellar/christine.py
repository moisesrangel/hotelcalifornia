import settings
import sys
import furby
import aiml

from skylark import fn
from multiprocessing import Process
from models import Bots


class Christine (object):
	def __init__(self):
		super(Christine , self).__init__()

	def talk(self, access_token, access_token_secret,last_reply,bot_id,k):
		f = furby.Furby()
		f.talk(access_token, access_token_secret,last_reply,bot_id,k)

	def select(self):
		#bots = [(bot.access_token,bot.access_token_secret,bot.last_reply,bot.id) for bot in Bots.where(id = 2).select()]

		k = aiml.Kernel()
		k.learn("aiml/default.aiml")
		k.learn("aiml/nombres.aiml")
		k.learn("aiml/numeros.aiml")
		k.learn("aiml/sara_srai.aiml")
		k.learn("aiml/sara.aiml")
		k.learn("aiml/sexo.aiml")

		bots = [(bot.access_token,bot.access_token_secret,bot.last_reply,bot.id) for bot in Bots.orderby(fn.rand()).getall()]
		for bot in bots:
			p = Process(target=self.talk, args=(bot[0],bot[1],bot[2],bot[3],k))
			p.start()
			p.join()

if __name__ == '__main__':
	print "I'm Christine, let me do the talking ;)"
	chris = Christine();
	chris.select()