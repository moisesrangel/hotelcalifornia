#set path=%path%;C:\python27
#set path=%path%;C:\python27\Scripts
import os
import requests
import bleach
import re
import nltk
import lxml

from slugify         import slugify
from bs4             import BeautifulSoup
from nltk.tokenize   import sent_tokenize
from lxml.html.clean import Cleaner
from lxml            import html

#Spanish Tagger
import spaghetti as spa

class AntonSzandor(object):
    """AntonSzandor, Creador de todo el conocimiento de Hotel California"""

    links         = []
    concepts      = []
    levels        = 1
    current_level = 0
    text          = ""

    def __init__(self):
        super(AntonSzandor, self).__init__()

    def getLinks(self, soup):
        for link in soup.find_all('a'):
            self.filterhref(link)

    def soupify(self, link):
        try:
            request = requests.get(link)
            html    = request.text
            return BeautifulSoup(html, "html5lib")
        except Exception, e:
            print e
            pass

    def filterhref(self, link):
        href     = link.get('href')
        if href is not None:
            filters  = ['jpg', 'jpeg', 'gif', 'png', 'pdf', 'xml', 'bmp']
            status   = True
            for filtero in filters:
                if href.endswith(filtero):
                    status = False
                    break

            if status == True:
                self.links.append(link)

    def processsentence(self, sentence):
        try:
            words = word_tokenize(sentence)
            for word in words:
                if(self.validateWord(word)):
                    #self.saveword(word)
                    print word
                #else:
                    #print "Out: " + word
        except Exception, e:
            print e
            pass

    def is_valid_url(self, url):
        import re
        regex = re.compile(
            r'^https?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        return url is not None and regex.search(url)

    def validateWord(self, word):

        status = True
        if word.isdigit():
            status = False

        if word[0].isdigit():
            status = False

        if len(word) < 4:
            status = False

        if self.is_valid_url(word):
            status = False

        #if status:
        #    keyw = Keywords.findone(keyword = word)
        #    if keyw:
        #        status = False

        return status

    #http://stackoverflow.com/questions/14095971/how-to-tweak-the-nltk-sentence-tokenizer
    def remember(self, text):
        book =  u''.join(text).encode('utf-8').strip()
        punkt_param = PunktParameters()
        punkt_param.abbrev_types = set(['dr', 'vs', 'mr', 'mrs', 'prof', 'inc', 'sr', 'sra', 'miss'])
        sentence_splitter = PunktSentenceTokenizer(punkt_param)
        sentences = sentence_splitter.tokenize(text)
        for sentence in sentences:
            line = u''.join(sentence).encode('utf-8').strip(' \t\n\r')
            line = os.linesep.join([s for s in line.splitlines() if s])
            #self.processsentence(line)

    def wordtokenizr(self, sentence):
        """
          Tags permitidos

          JJ: adjective or numeral, ordinal
          NNS: noun, common, plural
          VBP: verb, present tense, not 3rd person singular

          FW: Foreign Word // regularmente palabras localizadas

        """
        tokens       = nltk.word_tokenize(sentence)
        tags         = spa.pos_tag(tokens)
        valid_tokens = ["JJ", "NNS", "VBP", "FW"]
        output       = ""
        for tag in tags:
            print "tag: ", tag
            nltktag = tag[1]
            if nltktag in valid_tokens:

                output += tag[0]
                output += " "

        print "output: ", self.toutf8(output)

    def cleanemptylines(self, line):
        out = ""
        for l in line.split('\n'):
            if l.strip():
                out +=  l + '\n'
        return out

    def getsentences(self):
        sentences = sent_tokenize(self.text)
        for sen in sentences:
            sentence = self.cleanemptylines(sen.strip())
            print "sentencia: ", self.toutf8(sentence)
            sentence = self.wordtokenizr(sentence)


    def toutf8(self, text):
        return u''.join(text).encode('utf-8').strip()

    def geturltext(self, url):
        cleaner            = Cleaner()
        cleaner.javascript = True
        cleaner.style      = True

        tree      = html.parse(url)
        tree      = cleaner.clean_html(tree)
        self.text = re.sub(' +',' ', tree.getroot().text_content())


    def readabook(self, link_book):

        print "Leyendo... " + link_book

        #self.readbook = self.soupify(link_book)
        #self.getLinks(self.readbook)

        self.geturltext(link_book)
        self.getsentences()
        exit()

        #self.remember(self.readbook.text)

        self.current_level  = self.current_level + 1

        if(self.levels > self.current_level):
           for link in self.links:
            try:
               self.readabook(link.get('href'))
            except Exception, e:
                print e

if __name__ == '__main__':
    print "Bienvenido a Hotel California. Amabilidad hacia quienes la merecen, en lugar del amor malgastado en ingratos\r\n"
    Anton = AntonSzandor()
    Anton.readabook("http://www.instyle.mx/")

