class AntonSzandor(object):
    """AntonSzandor, Creador de todo el conocimiento de Hotel California"""

    links         = []
    concepts      = []
    levels        = 1
    current_level = 0
    text          = ""

    def __init__(self):
        super(AntonSzandor, self).__init__()

    def toutf8(self, text):
        return u''.join(text).encode('utf-8').strip()

    def readabook(self, link_book):

        print "Leyendo... " + link_book

        self.geturltext(link_book)
        self.getsentences()
        exit()


if __name__ == '__main__':
    print "Bienvenido a Hotel California. Amabilidad hacia quienes la merecen, en lugar del amor malgastado en ingratos\r\n"
    Anton = AntonSzandor()
    Anton.readabook("http://www.instyle.mx/")

