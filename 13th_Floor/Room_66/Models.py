import MySQLdb
import settings

from skylark import Database, Model, Field, PrimaryKey, ForeignKey, fn

class Profiles(Model):
    name 		= Field()
    root 		= Field()
    tags 		= Field()
    created_at  = Field()

class SitesRoot(Model):
    profileid   = Field()
    url         = Field()
    active      = Field()

class Tweets(Model):
    profileid   = Field()
    twitter_id  = Field()
    text        = Field()
    searchterm  = Field()
    created_at  = Field()


Database.config(db=settings.database, user=settings.user, passwd=settings.password)