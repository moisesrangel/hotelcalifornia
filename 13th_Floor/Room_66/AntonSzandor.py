from Reader  import Reader
from Models  import Profiles, SitesRoot, Tweets
from bs4     import BeautifulSoup

import requests

class AntonSzandor(object):
    """AntonSzandor, Creador de todo el conocimiento de Hotel California"""

    LINK_DEEP    = 5
    CURRENT_DEEP = 0
    root_site    = ""
    links        = []

    def GetSiteRoot(self):
        qry = SitesRoot.where(active=1).select()
        result = qry.execute()
        self.sites = result.all()

    def soupify(self, link):
        try:
            request = requests.get(link)
            html    = request.text
            return BeautifulSoup(html, "html5lib")
        except Exception, e:
            print e
            pass

    def GetLinks(self, link):        
        soup = self.soupify(link)        
        for link in soup.find_all('a'):
            href     = link.get('href')
            if href is not None:
                filters  = ['jpg', 'jpeg', 'gif', 'png', 'pdf', 'xml', 'bmp']
                status   = True
                for filtero in filters:
                    if href.endswith(filtero):
                        status = False
                        break

                if status == True:
                    slash    = self.CurrentSite.url[-1:]
                    rootsite = self.CurrentSite.url if slash != "/" else self.CurrentSite.url[:-1]
                    if rootsite != href:
                        #self.GetLinks(href)   #recursive attempt         
                        if href not in self.links:
                            self.links.append(href) 


        

    def DiggLinks(self, soup):
        pass

    def filterhref(self, link):
        pass                   

    def ToUTF8(self, text):
        return u''.join(text).encode('utf-8').strip()

    def SaveTweets(self, rows):
        for row in rows:
            if(row.language == "es"):                
                tweet = Tweets()
                tweet.profileid = self.CurrentSite.profileid
                tweet.twitter_id = row.id
                tweet.text = self.ToUTF8(row.text)
                tweet.save()

    def GenerateKeywords(self):
        for link in self.links:
            try:
                reader = Reader(link)
                #reader.GetTweets()
                #self.SaveTweets(reader.tweets)

            except Exception, e:
                print e
                pass

    def Main(self):
        self.GetSiteRoot()
        for site in self.sites:
            self.CurrentSite = site
            self.GetLinks(site.url)

        self.GenerateKeywords()

if __name__ == '__main__':
    print "Bienvenido a Hotel California. Amabilidad hacia quienes la merecen, en lugar del amor malgastado en ingratos\r\n"
    instance = AntonSzandor()
    instance.Main()


    #reader = Reader("http://www.vanguardia.com.mx/articulo/para-que-servira-el-911")
    #reader.GetTweets()

