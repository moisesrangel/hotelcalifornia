import settings

from skylark import Database, Model, Field, PrimaryKey, ForeignKey, fn

class Bots(Model):
    id                  = PrimaryKey()
    name                = Field()
    username            = Field()
    twitter_id          = Field()
    access_token        = Field()
    access_token_secret = Field()
    last_reply          = Field()

class Profile(Model):
    id                   = PrimaryKey()
    bot_id               = ForeignKey(Bots.id)
    description          = Field()
    location             = Field()
    url                  = Field()
    background_color     = Field()
    text_color           = Field()
    link_color           = Field()
    sidebar_fill_color   = Field()
    sidebar_border_color = Field()
    profile_banner       = Field()
    profile_image        = Field()

class ReplyLog(Model):
    id                      = PrimaryKey()
    bot_id                  = ForeignKey(Bots.id)
    in_reply_to_status_id   = Field()
    in_reply_to_user_id     = Field()
    in_reply_to_screen_name = Field()
    text                    = Field()
    reply_text              = Field()
    reply_date              = Field()

class Content(Model):
    id        = Field()
    source    = Field()
    text      = Field()
    origin    = Field()
    keywords  = Field()

class Keywords(Model):
    id        = Field()
    keyword   = Field()

class Trends(Model):
    id         = Field()
    Name       = Field()
    user_id    = Field()
    start_date = Field()
    end_date   = Field()
    status     = Field()

class TrendsSettings(Model):
    id         = Field()
    key        = Field()
    value      = Field()

class TrendsBots(Model):
    trends_id = ForeignKey(Trends.id)
    bots_id   = ForeignKey(Bots.id)

class TrendsTweets(Model):
    id        = Field()
    trends_id = ForeignKey(Trends.id)
    text      = Field()
    tweet_id  = Field()
    types     = Field()
    status    = Field()


Database.config(db=settings.database, user=settings.user, passwd=settings.password, charset='utf8')