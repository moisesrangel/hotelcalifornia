import tweepy

def create_favorite(favorite_id,access_token,access_token_secret,auth):
	auth.secure = True
	auth.set_access_token(access_token, access_token_secret)

	api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

	try:
		result = api.create_favorite(favorite_id)
	except tweepy.TweepError as e:
 		result = e.message;

	return result

def follow(username,access_token,access_token_secret,auth):
	auth.secure = True
	auth.set_access_token(access_token, access_token_secret)

	api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

	try:
		result = api.create_friendship(username)
	except tweepy.TweepError as e:
		result = e.message;
	return result

def create_retweet(retweet,access_token,access_token_secret,auth):
	auth.secure = True
	auth.set_access_token(access_token, access_token_secret)

	api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

	try:
		result = api.retweet(retweet)
	except tweepy.TweepError as e:
		result = e.message;
	return result