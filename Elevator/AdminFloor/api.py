import ast
import json
import string
import datetime
import settings
import tweepy
import subprocess
import ssl


from twitter import create_favorite
from twitter import follow
from twitter import create_retweet

from models import Bots, Profile
from skylark import fn


from flask import Flask, jsonify, request
from flask.ext import restful

app = Flask(__name__)
api = restful.Api(app)

class Follow(restful.Resource):
    def post(self):
		username = request.form['username']
		count = request.form['count']
		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)

		values = []
		bots = Bots.orderby(fn.rand(),desc=False).limit(count).getall()
		for bot in bots:
			values.append(follow(username,bot.access_token,bot.access_token_secret,auth))

		return values

class Favorite(restful.Resource):
    def post(self):
		id_message = request.form['id_message']
		count = request.form['count']

		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)

		values = []
		bots = Bots.orderby(fn.rand(),desc=False).limit(count).getall()
		for bot in bots:
			values.append(create_favorite(id_message,bot.access_token,bot.access_token_secret,auth))

		return values

class Retweet(restful.Resource):
    def post(self):
		id_message = request.form['id_message']
		count = request.form['count']

		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)

		values = []
		bots = Bots.orderby(fn.rand(),desc=False).limit(count).getall()
		for bot in bots:
			values.append(create_retweet(id_message,bot.access_token,bot.access_token_secret,auth))

		return values

class UpdateProfile(restful.Resource):
    def post(self):

		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)

		query = Bots.where(id=request.form['bot_id']).select()
		bot = query.execute()
		bot = bot.one()

		auth.secure = True
		auth.set_access_token(bot.access_token, bot.access_token_secret)

		api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

		query = Profile.where(bot_id=bot.id).select()
		p = query.execute()
		p = p.one()
		values = []

		#values.append((p.profile_link_color,p.text_color,p.link_color,p.sidebar_fill_color,p.sidebar_border_color))

		values.append(api.update_profile(bot.name,'','Mexico',p.description))

		try:
			api.update_profile_banner(p.profile_banner)
		except tweepy.TweepError as e:
			values.append( e.message );

		try:
			api.update_profile_image(p.profile_image)
		except tweepy.TweepError as e:
			values.append( e.message );

		return values

class Username(restful.Resource):
    def get(self, username):
		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)
		api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())
		user = api.get_user(username)
		return user

class UserTimeline(restful.Resource):
    def get(self, username):
		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret)
		api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())
		user = api.user_timeline(username)
		return user




api.add_resource(Follow, '/follow/')
api.add_resource(Favorite, '/favorite/')
api.add_resource(Retweet, '/retweet/')
api.add_resource(UpdateProfile, '/update_profile')
api.add_resource(Username, '/username/<string:username>')
api.add_resource(UserTimeline, '/usertimeline/<string:username>')



if __name__ == '__main__':
    #app.run(host='192.168.1.74',debug=True)
    app.run(host='127.0.0.1',debug=True)

