import settings
from skylark import Database, Model, Field, PrimaryKey, ForeignKey, fn

#Bots Model
class Bots(Model):
    id                  = PrimaryKey()
    name                = Field()
    username            = Field()
    twitter_id          = Field()
    access_token        = Field()
    access_token_secret = Field()
    last_reply          = Field()

class Personalities(Model):
    id                  = PrimaryKey()
    name                = Field()
    keyname             = Field()s
    description         = Field()
    active              = Field()

class BotsPersonalities(Model):
    id                  = PrimaryKey()
    bot_id              = Field()
    personality_id      = Field()

class Content(Model):
    id        = PrimaryKey()
    source    = Field()
    text      = Field()
    origin    = Field()
    keywords  = Field()

class Keywords(Model):
    id        = Field()
    keyword   = Field()

class Trends(Model):
    id         = PrimaryKey()
    Name       = Field()
    user_id    = ForeignKey(Trends.id)
    start_date = Field()
    end_date   = Field()
    status     = Field()

class TrendsSettings(Model):
    id         = PrimaryKey()
    key        = Field()
    value      = Field()

class TrendsBots(Model):
    trends_id = ForeignKey(Trends.id)
    bots_id   = ForeignKey(Bots.id)

class TrendsTweets(Model):
    id        = PrimaryKey()
    trends_id = ForeignKey(Trends.id)
    text      = Field()
    tweet_id  = Field()
    types     = Field()
    status    = Field()

#Final User Models
class Users(Model):
    id         = PrimaryKey()
    name       = Field()
    last_name  = Field()
    email      = Field()
    password   = Field()
    created_at = Field()

Database.config(db=settings.database, user=settings.user, passwd=settings.password, charset='utf8')